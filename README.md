[View Rendered Example](https://bitbucket.org/mastercontrol/plant-common/src/master/example.puml?viewer=render-plantuml-file) 

(MUST HAVE [PlantUML Diagram Viewer plugin](https://marketplace.atlassian.com/apps/1220172/bitbucket-plantuml-viewer?hosting=cloud&tab=overview))

## Use as a Shared Resource
Include into any PlantUML diagram using:

`!includeurl https://bitbucket.org/mastercontrol/plant-common/raw/master/plant-common.iuml`

## Available Objects
- ANGULAR
- BASH_TERMINAL
- BROWSER
- DOCKER
- FOLDER
- JAVA
- LAPTOP
- MC
- WEBSITE
- WINDOWS_TERMINAL

## Available Functions

#### `keepspace(<input_string>)`
Keeps raw spacing when converting `input_string` to xml

i.e. wraps with `<text xml:space="preserve">`

#### `urlimage(<host>, <path>)`
Returns `host + path` as src in `<img>` tag

#### `image(<image_file>)`
Returns `image_file` from [plant-common/images](https://bitbucket.org/mastercontrol/plant-common/src/master/images/) as `src` in `<img>` tag

#### `icon(<icon_path>)`
Returns `icon_path` from [Roemer/plantuml-office](https://github.com/Roemer/plantuml-office/tree/master/office2014) as `src` in `<img>` tag
